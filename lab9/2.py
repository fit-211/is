import asyncio
import aiohttp


async def get_ip(service_name, url):
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(url) as response:
                ip = await response.text()
                return service_name, ip
        except:
            return None, None


async def main():
    services = [
        ("ipify", "https://api.ipify.org"),
        ("ip-api", "http://ip-api.com/plain"),
        # add more services here if needed
    ]
    tasks = []
    for service_name, url in services:
        task = asyncio.ensure_future(get_ip(service_name, url))
        tasks.append(task)
    responses = await asyncio.gather(*tasks, return_exceptions=True)
    for r in responses:
        if r and r[1]:
            print(f"Your IP address is {r[1]} from {r[0]}")
            break


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
