import telebot
import config
import datetime

from telebot import types

bot = telebot.TeleBot(config.TOKEN)


@bot.message_handler(commands=['start'])
def welcome(message):
    sti = open('welcome.webp', 'rb')

    bot.send_sticker(message.chat.id, sti)

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    but1 = types.KeyboardButton("Time")
    but2 = types.KeyboardButton("Date")
    markup.add(but1, but2)

    bot.send_message(message.chat.id, "саллам аллейкум", reply_markup=markup)


@bot.message_handler(content_types=['text'])
def mes(message):
    time = datetime.datetime.now()
    if message.chat.type == 'private':
        if message.text == 'Time':
            bot.send_message(message.chat.id, time.strftime("%H:%M:%S"))
        elif message.text == 'Date':
            bot.send_message(message.chat.id, time.strftime("%Y-%M-%D"))
        else:
            bot.send_message(message.chat.id, 'Я не знаю, что ответить')


bot.polling(none_stop=True)
