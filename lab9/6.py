import telebot
import config
import time
import random

from telebot import types

bot = telebot.TeleBot(config.TOKEN)

# markups
markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
but1 = types.KeyboardButton("/dice")
but2 = types.KeyboardButton("/timer")
markup.add(but1, but2)

# dice button
dices_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
d1 = types.KeyboardButton("6-гранный кубик")
d2 = types.KeyboardButton("2 6-гранных кубика")
d3 = types.KeyboardButton("20-гранный кубик")
d4 = types.KeyboardButton("Назад")
dices_markup.add(d1, d2, d3, d4)
# /dice button

# timer but
timer_markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
t1 = types.KeyboardButton("30 секунд")
t2 = types.KeyboardButton("1 минута")
t3 = types.KeyboardButton("5 минут")
t4 = types.KeyboardButton("Назад")
timer_markup.add(t1, t2, t3, t4)
# /timer but
# during timer
t_mur = types.ReplyKeyboardMarkup(resize_keyboard=True)
t_mur.add(types.KeyboardButton("/close"))


# /during timer

@bot.message_handler(commands=['start'])
def welcome(message):
    sti = open('welcome.webp', 'rb')

    bot.send_sticker(message.chat.id, sti)

    bot.send_message(message.chat.id, "привет, я чат-бот для настольных игр", reply_markup=markup)


@bot.message_handler(content_types=['text'])
def mes(message):
    def timer(sec):
        bot.send_message(message.chat.id, f"Таймер установлен на {sec} секунд")
        time.sleep(sec)
        bot.send_message(message.chat.id, "{sec} секунд истекли!")

    # /timer func

    if message.chat.type == 'private':
        if message.text == '/dice':
            bot.send_message(message.chat.id, "Какой кубик бросим?", reply_markup=dices_markup)
        elif message.text == '/timer':
            bot.send_message(message.chat.id, "На сколько поставить таймер?", reply_markup=timer_markup)
        elif message.text == '30 секунд':
            timer(30)
        elif message.text == '1 минута':
            timer(60)
        elif message.text == '5 минут':
            timer(150)
        elif message.text == '6-гранный кубик':
            bot.send_message(message.chat.id, str(random.randint(1, 6)))
        elif message.text == '2 6-гранных кубика':
            bot.send_message(message.chat.id, str(random.randint(1, 6)))
            bot.send_message(message.chat.id, str(random.randint(1, 6)))
        elif message.text == '20-гранный кубик':
            bot.send_message(message.chat.id, str(random.randint(1, 20)))
        elif message.text == 'Назад':
            bot.send_message(message.chat.id, 'Возвращаемся назад...', reply_markup=markup)
        else:
            bot.send_message(message.chat.id, 'Я не знаю, что ответить')


bot.polling(none_stop=True)
